package com.mobox.budabi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobox.budabi.R;
import com.mobox.budabi.utils.AllOpersComponents;

import java.util.List;

/**
 * Created by beeLAN on 25.11.2015.
 */
public class AllOpersAdapter extends BaseAdapter {
    private static final String BONUS = "BONUS";
    private static final String PENALTY = "PENALTY";
    private static final String CASHIN = "CASHIN";
    private static final String CASHOUT = "CASHOUT";
    private static final String EX = "EX";
    private static final String EXIN = "EXIN";
    private static final String TR = "TR";
    private static final String TRIN = "TRIN";
    private static final String BUY = "BUY";
    private static final String SELL = "SELL";
    private static final String BUY2 = "BUY2";
    private static final String SELL2 = "SELL2";
    private static final String REF = "REF";
    private static final String GIVE = "GIVE";
    private static final String TAKE = "TAKE";
    private static final String CALCIN = "CALCIN";
    private static final String CALCOUT = "CALCOUT";
    private  List<AllOpersComponents> list;
    private  LayoutInflater layoutInflater;
    private  Context mContext;

    public AllOpersAdapter(List<AllOpersComponents> list, Context context) {
        this.list = list;
        this.mContext = context;
    }

    static class ViewHolder{
        private ImageView image;
        private TextView header;
        private TextView date;
        private TextView sum;
        private TextView currency;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.all_operations_list_items, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.aoimage);
            viewHolder.header = (TextView) convertView.findViewById(R.id.aoheader);
            viewHolder.date = (TextView) convertView.findViewById(R.id.aodate);
            viewHolder.sum = (TextView) convertView.findViewById(R.id.aosum);
            viewHolder.currency = (TextView) convertView.findViewById(R.id.aocurrency);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AllOpersComponents list = getConfirmList(position);

        switch (list.getState()){
            case "0":
                viewHolder.image.setBackgroundResource(R.drawable.time);
                viewHolder.header.setTextColor(convertView.getResources().getColor(R.color.highlight_color));
                break;
            case "1":
                viewHolder.image.setBackgroundResource(R.drawable.time);
                viewHolder.header.setTextColor(convertView.getResources().getColor(R.color.highlight_color));
                break;
            case "2":
                viewHolder.image.setBackgroundResource(R.drawable.time);
                viewHolder.header.setTextColor(convertView.getResources().getColor(R.color.highlight_color));
                break;
            case "3":
                viewHolder.image.setBackgroundResource(R.drawable.check);
                viewHolder.header.setTextColor(convertView.getResources().getColor(R.color.colorAccent));
                break;
            case "4":
                viewHolder.image.setBackgroundResource(R.drawable.close);
                viewHolder.header.setTextColor(convertView.getResources().getColor(R.color.submit_attantion_color));
                break;
            case "5":
                viewHolder.image.setBackgroundResource(R.drawable.close);
                viewHolder.header.setTextColor(convertView.getResources().getColor(R.color.submit_attantion_color));
                break;
        }

        switch (list.getHeader()){
            case BONUS:
                viewHolder.header.setText("Бонус");
                break;
            case PENALTY:
                viewHolder.header.setText("Штраф");
                break;
            case CASHIN:
                viewHolder.header.setText("Пополнение");
                break;
            case CASHOUT:
                viewHolder.header.setText("Вывод");
                break;
            case EX:
                viewHolder.header.setText("Исходящий обмен");
                break;
            case EXIN:
                viewHolder.header.setText("Входящий обмен");
                break;
            case TR:
                viewHolder.header.setText("Перевод");
                break;
            case TRIN:
                viewHolder.header.setText("Приход");
                break;
            case BUY:
                viewHolder.header.setText("Продажа");
                break;
            case SELL:
                viewHolder.header.setText("Продажа");
                break;
            case BUY2:
                viewHolder.header.setText("Страховка");
                break;
            case SELL2:
                viewHolder.header.setText("Благотворительность");
                break;
            case REF:
                viewHolder.header.setText("Рефские");
                break;
            case GIVE:
                viewHolder.header.setText("Вклад");
                break;
            case TAKE:
                viewHolder.header.setText("Снятие");
                break;
            case CALCIN:
                viewHolder.header.setText("Начисление");
                break;
            case CALCOUT:
                viewHolder.header.setText("Отчисление");
                break;
        }

        switch (list.getCurrency()){
            case "2":
                viewHolder.currency.setText("EUR");
                break;
            case "3":
                viewHolder.currency.setText("USD");
                break;
            case "4":
                viewHolder.currency.setText("RUB");
                break;
            case "6":
                viewHolder.currency.setText("UAH");
                break;
            case "8":
                viewHolder.currency.setText("PLN");
                break;
        }
        viewHolder.date.setText(list.getDate());
        viewHolder.sum.setText(list.getSum());

        return convertView;
    }

    private AllOpersComponents getConfirmList(int position) {
        return (AllOpersComponents) getItem(position);
    }

}
