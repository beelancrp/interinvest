package com.mobox.budabi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobox.budabi.R;
import com.mobox.budabi.utils.AllDepositsComponents;

import java.util.List;

public class AllDepositAdapter extends BaseAdapter {

    private List<AllDepositsComponents> list;
    private LayoutInflater layoutInflater;
    private Context mContext;


    public AllDepositAdapter(List<AllDepositsComponents> list, Context context) {
        this.list = list;
        this.mContext = context;
    }

    static  class ViewHolder{
        TextView header;
        TextView startDate;
        TextView currency;
        TextView sum;
        TextView plan;
        TextView lastCalculation;
        TextView calculations;
        TextView calculate;
        TextView kapnetTomorrow;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.all_deposits_list_items, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.header = (TextView) convertView.findViewById(R.id.adheader);
            viewHolder.startDate =(TextView) convertView.findViewById(R.id.addate);
            viewHolder.currency = (TextView) convertView.findViewById(R.id.adcurrency);
            viewHolder.sum = (TextView) convertView.findViewById(R.id.adsum);
            viewHolder.plan = (TextView) convertView.findViewById(R.id.adplan);
            viewHolder.lastCalculation = (TextView) convertView.findViewById(R.id.adlastcalc);
            viewHolder.calculations = (TextView) convertView.findViewById(R.id.adcalcs);
            viewHolder.calculate = (TextView) convertView.findViewById(R.id.adcalc);
            viewHolder.kapnetTomorrow = (TextView) convertView.findViewById(R.id.adkapnet);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AllDepositsComponents list = getConfirmList(position);
        switch (list.getHeader()){
            case 0:
                viewHolder.header.setText("Отменен");
                break;
            case 1:
                viewHolder.header.setText("Активен");
                break;
            case 2:
                viewHolder.header.setText("Окончен");
                break;
            case 3:
                viewHolder.header.setText("Закрыт");
                break;
        }
        viewHolder.startDate.setText(list.getStartDate());
        switch (list.getCurrencyId()){
            case 2:
                viewHolder.currency.setText("EUR");
                break;
            case 3:
                viewHolder.currency.setText("USD");
                break;
            case 4:
                viewHolder.currency.setText("RUB");
                break;
            case 6:
                viewHolder.currency.setText("UAH");
                break;
            case 8:
                viewHolder.currency.setText("PLN");
                break;
        }
        viewHolder.sum.setText(list.getSum());
        viewHolder.plan.setText(list.getPlan());
        viewHolder.lastCalculation.setText(list.getLastCalculation());
        viewHolder.calculations.setText(list.getCalculations());
        viewHolder.calculate.setText(list.getCalculate());
        viewHolder.kapnetTomorrow.setText(list.getKapnetTomorrow());

        return convertView;
    }

    private AllDepositsComponents getConfirmList(int position){
        return (AllDepositsComponents) getItem(position);
    }
}
