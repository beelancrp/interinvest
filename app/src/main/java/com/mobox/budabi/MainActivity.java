package com.mobox.budabi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.activities.AllDepositsActivity;
import com.mobox.budabi.activities.AllOpersActivity;
import com.mobox.budabi.activities.DepositActivity;
import com.mobox.budabi.activities.LoginActivity;
import com.mobox.budabi.activities.UserInfoActivity;
import com.mobox.budabi.activities.VuvodActivity;
import com.mobox.budabi.utils.CabinetInfo;
import com.mobox.budabi.utils.CallLoadnigView;
import com.mobox.budabi.utils.RequestCodes;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final int ACTIVITY_MAIN = R.layout.activity_main;
    public static final int CABINET = R.string.cabinet;
    public static final boolean SHOW_TITLE = false;
    public static final int MENU_LOGOUT = R.id.menu_item_logout;
    public static final int IC_KEYBOARD_BACKSPACE  = R.drawable.ic_arrow_left;
//    public static final int CABINET_USD_TAB = R.id.cabinet_USD_tab;
//    public static final int CABINET_UAH_TAB = R.id.cabinet_UAH_tab;
    public static final int CABINET_RUB_TAB = R.id.cabinet_RUB_tab;
    public static final int CABINET_EUR_TAB = R.id.cabinet_EUR_tab;
//    public static final int CABINET_PLN_TAB = R.id.cabinet_PLN_tab;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";


    //    private FragmentManager mFragmentManager;
    private Toolbar toolbar;
//    private TextView mUSDTab;
//    private TextView mUAHTab;
    private TextView mRUBTab;
    private TextView mEURTab;
//    private TextView mPLNTab;
    private TextView mBalance;
//    private TextView mBalanceIn;
    private TextView exchangeRate;

    private User userInfo;
    private CabinetInfo cabinetInfo;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView mInformationTextView;
    private boolean loginWasSucsses = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(ACTIVITY_MAIN);
        initLogin();

        initToolBar();
    }

    @Override
    protected void onRestart() {
        if(!UserLocalStore.isUserLoggedIn()){
            initLogin();
        }
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null && loginWasSucsses){
            CallLoadnigView.getInstance(MainActivity.this, R.id.cabinet_loginLoadingView).showLoadingView();
            userInfo = UserLocalStore.getUserData();
            doCabinetInfoRequest(userInfo.getParamsForCabinetRequest());
        }else {
            Toast.makeText(this, "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
        }
        super.onRestart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case MENU_LOGOUT:
                doLogOut();
        }
        return true;
    }

    private void initTabs (){
////        mUSDTab = (TextView) findViewById(CABINET_USD_TAB);
////        mUAHTab = (TextView) findViewById(CABINET_UAH_TAB);
        mRUBTab = (TextView) findViewById(CABINET_RUB_TAB);
        mEURTab = (TextView) findViewById(CABINET_EUR_TAB);
////        mPLNTab = (TextView) findViewById(CABINET_PLN_TAB);
        mBalance = (TextView) findViewById(R.id.cabinet_your_balance);
////        mBalanceIn = (TextView) findViewById(R.id.cabinet_balance_in);
//        exchangeRate = (TextView) findViewById(R.id.cabinet_course);
//        exchangeRate.setText("КУРС $ : " + cabinetInfo.getExchange_rate());
        onTabClick(mRUBTab);
    }

    private void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(CABINET);
        setSupportActionBar(toolbar);
    }

    private void initLogin() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivityForResult(intent, RequestCodes.REQUEST_CODE_LOGIN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case RequestCodes.REQUEST_CODE_LOGIN:
                    loginWasSucsses = true;
            }
        }else {
            switch (resultCode){
                case RequestCodes.REQUEST_CODE_LOGIN:
                    onPause();
                    initLogin();
            }
        }
    }

    public void doLogOut (){
        if(UserLocalStore.isUserLoggedIn()){
            UserLocalStore.cleanUserData();
            initLogin();
        }
    }

    public void onCabinetButtonClick(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.cabinetBtnPopolnit:
//                intent = new Intent(this, PayActivity.class);
//                startActivity(intent);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Внимание!")
                        .setMessage("Пополнение возможно только через сайт.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
//            case R.id.cabinetBtnExchange:
//                intent = new Intent(this, ExchangeActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.cabinetBtnTransaction:
//                intent = new Intent(this, TransactionActivity.class);
//                startActivity(intent);
//                break;
            case R.id.cabinetBtnDeposit:
                intent = new Intent(this, DepositActivity.class);
                startActivity(intent);
                break;
            case R.id.cabinetBtnVuvod:
                intent = new Intent(this, VuvodActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_userInfo:
                intent = new Intent(this, UserInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_allDeposits:
                intent = new Intent(this, AllDepositsActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_allOpers:
                intent = new Intent(this, AllOpersActivity.class);
                startActivity(intent);
                break;
//            case R.id.cabinetBtnMore:
//                intent = new Intent(this, MoreActivity.class);
//                startActivity(intent);
//                break;
        }
    }

    public void onTabClick(View view){
        switch (view.getId()){
//            case CABINET_USD_TAB:
//                mUSDTab.setTextColor(getResources().getColor(R.color.colorAccent));
//                mUAHTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mRUBTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mEURTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mPLNTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mBalanceIn.setText("Баланс в долларах");
//                mBalance.setText(cabinetInfo.getUsd() + " usd");
//                break;
//            case CABINET_UAH_TAB:
//                mUSDTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mUAHTab.setTextColor(getResources().getColor(R.color.colorAccent));
//                mRUBTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mEURTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mPLNTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mBalanceIn.setText("Баланс в гривнах");
//                mBalance.setText(cabinetInfo.getUah() + " грн");
//                break;
            case CABINET_RUB_TAB:
//                mUSDTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mUAHTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
                mRUBTab.setTextColor(getResources().getColor(R.color.colorAccent));
                mEURTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mPLNTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mBalanceIn.setText("Баланс в рублях");
                mBalance.setText(cabinetInfo.getRus() + " $");
                break;
            case CABINET_EUR_TAB:
//                mUSDTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mUAHTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
                mRUBTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
                mEURTab.setTextColor(getResources().getColor(R.color.colorAccent));
//                mPLNTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mBalanceIn.setText("Баланс в евро");
                mBalance.setText(cabinetInfo.getEur() + " $");
                break;
//            case CABINET_PLN_TAB:
//                mUSDTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mUAHTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mRUBTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mEURTab.setTextColor(getResources().getColor(R.color.cabinetTabTextColor));
//                mPLNTab.setTextColor(getResources().getColor(R.color.colorAccent));
//                mBalanceIn.setText("Баланс в злотых");
//                mBalance.setText(cabinetInfo.getPln() + " pln");
//                break;
        }
    }

    public  void doCabinetInfoRequest(final Map<String, String> params) {
        final String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                JSONObject balance = data.getJSONObject("balance");
                                String exchange_rate = data.getString("exchange_rates");
                                cabinetInfo = new CabinetInfo(
                                        balance.getString("PERFECT"),
                                        balance.getString("PAYEER"));
                                initTabs();
                                CallLoadnigView.getInstance(MainActivity.this, R.id.cabinet_loginLoadingView).hideLoadingView();
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, MainActivity.this, MainActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        doLogOut();
        super.onNewIntent(intent);
    }

//    private boolean checkPlayServices() {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (apiAvailability.isUserResolvableError(resultCode)) {
//                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
//                        .show();
//            } else {
//                Log.i(TAG, "This device is not supported.");
//                finish();
//            }
//            return false;
//        }
//        return true;
//    }

}
