package com.mobox.budabi.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.LayoutFragmentAdapter;
import com.mobox.budabi.utils.FragmentComponentsList;
import com.mobox.budabi.utils.NewDepoResponse;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by beeLAN on 07.12.2015.
 */
public class DepositConfirmFragment extends Fragment {

    public static final String TAG = "DepositConfirmFragment";
    private String sum;
    private String paySystem;
    private String paySystemId;
    private User userInfo = UserLocalStore.getUserData();
    List<FragmentComponentsList> list = new ArrayList<>();
    LayoutFragmentAdapter adapter;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_deposit_confirm, null, false);
        Bundle bundle = getArguments();
        if(bundle != null){
            String[] s = bundle.getStringArray("newDepo");
            sum = s[0];
            paySystem = s[1];
        }

        paySystemId = getPaySystemId(paySystem);

        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null){
            doNewDepoRequest();
        }else {
            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG ).show();
        }
        listView = (ListView) view.findViewById(R.id.deposit_confirm_list);
        adapter = new LayoutFragmentAdapter(getContext(), R.layout.confirm_list_item, list);
        listView.setAdapter(adapter);
        return view;
    }
    private String getPaySystemId(String system){
        String res;
        switch (system){
            case "Perfect":
                res = "3";
                return res;
//            case "Гривна":
//                res = "6";
//                return res;
            case "Payeer":
                res = "4";
                return res;
//            case "Euro":
//                res = "2";
//                return res;
//            case "Злотых":
//                res = "8";
//                return res;
            default:
                res = "";
                return res;
        }
    }

    public  void doNewDepoRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                NewDepoResponse depoResponse = new NewDepoResponse(
                                        data.getString("dZD"),
                                        data.getString("pName"),
                                        data.getString("pNPer")
                                );
                                list.add(new FragmentComponentsList(1, "Сумма" , depoResponse.getSum()));
                                list.add(new FragmentComponentsList(2, "План", depoResponse.getPaySystem()));
                                list.add(new FragmentComponentsList(3, "Начислений", depoResponse.getCalculate()));
                                adapter.notifyDataSetChanged();
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "depo_new");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                params.put("cid", paySystemId);
                params.put("sum", sum);
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
