package com.mobox.budabi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobox.budabi.R;

import fr.ganfra.materialspinner.MaterialSpinner;


public class VuvodMainFragment extends Fragment {
    public static final String TAG = "VuvodMainFragment";
    public static final int VUVOD_FRAGMENT_CONTAINER = R.id.vuvod_fragment_container;


    private Button submit;
    private EditText sum;
    private String paySystem = "";
    private String[] ITEMS = {"Perfect", "Payeer"};
    private FragmentManager mFragmentManager;
    private VuvodConfirmFragment confirmFragment = new VuvodConfirmFragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vuvod_main, null);
        submit = (Button) view.findViewById(R.id.btn_vuvod);
        sum    = (EditText) view.findViewById(R.id.input_vuvod_summ);
        mFragmentManager = getActivity().getSupportFragmentManager();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.drop_down_list, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.vuvod_spinner);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    paySystem = ITEMS[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                paySystem = null;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });

        return view;
    }

    public static String getName() {
        return TAG;
    }

    public void onSubmit() {
        Bundle bundle = new Bundle();
        String lSum = sum.getText().toString();
        if((lSum.isEmpty() && paySystem == "") || lSum.isEmpty() || paySystem == ""){
            Toast.makeText(getContext(), "Все поля должны быть заполнены.", Toast.LENGTH_LONG).show();
        }else {
            bundle.putStringArray("vuvodData", new String[]{lSum, paySystem});
            confirmFragment.setArguments(bundle);
            mFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(VUVOD_FRAGMENT_CONTAINER, confirmFragment, confirmFragment.getName())
                    .commit();

        }

    }
}
