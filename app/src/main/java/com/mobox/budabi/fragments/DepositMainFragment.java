package com.mobox.budabi.fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.DepositAdapter;
import com.mobox.budabi.utils.CustomEditText;
import com.mobox.budabi.utils.DepositComponentsList;
import com.mobox.budabi.utils.DepositResponseVars;
import com.mobox.budabi.utils.NonScrollListView;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ganfra.materialspinner.MaterialSpinner;


public class DepositMainFragment extends Fragment {
    public static final int BTN_DEPOSIT_SUBMIT           = R.id.btn_deposit;
    public static final int DEPOSIT_FRAGMENT_CONTAINER  = R.id.deposit_fragment_container;
    public static final String TAG = "DepositMainFragment";

    private User userInfo = UserLocalStore.getUserData();
    private List<DepositComponentsList> itemList;
    private DepositAdapter adapter1;
    private CustomEditText sum;
    private String paySystem = "";
    private String[] ITEMS = {"Perfect", "Payeer"};
    private FragmentManager mFragmentManager;
    private DepositConfirmFragment confirmFragment = new DepositConfirmFragment();
    private Button submit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deposit_main, null);
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null){
            doDepoInfoRequest();
        }else {
            Toast.makeText(getContext(), "Подключение к интернету отсутствуетю", Toast.LENGTH_LONG).show();
        }
        mFragmentManager = getActivity().getSupportFragmentManager();
        submit = (Button) view.findViewById(BTN_DEPOSIT_SUBMIT);
        sum = (CustomEditText) view.findViewById(R.id.input_deposit_number);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.drop_down_list, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.deposit_spinner);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    paySystem = ITEMS[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                paySystem = null;
            }
        });

        itemList = new ArrayList<>();
        NonScrollListView listView = (NonScrollListView) view.findViewById(R.id.deposit_confirm_list);
        adapter1 = new DepositAdapter(itemList, getContext(), R.layout.deposit_list_items);
        listView.setAdapter(adapter1);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit(v);
            }
        });


        return view;
    }


    public void onSubmit(View view) {
        switch (view.getId()){
            case BTN_DEPOSIT_SUBMIT:
                String summ = sum.getText().toString();
                Bundle bundle = new Bundle();
                if((summ.isEmpty() && paySystem == "") || summ.isEmpty() || paySystem == ""){
                    Toast.makeText(getContext(), "Все поля должны быть заполнены.", Toast.LENGTH_LONG).show();
                }
//                else if (summ == "") {
//                    Toast.makeText(getContext(), "Все поля должны быть заполнены.", Toast.LENGTH_LONG).show();
//                }else if(paySystem == "") {
//                    Toast.makeText(getContext(), "Все поля должны быть заполнены.", Toast.LENGTH_LONG).show();
//                }
                else {
                    bundle.putStringArray("newDepo", new String[]{sum.getText().toString(), paySystem});
                    confirmFragment.setArguments(bundle);
                    mFragmentManager.beginTransaction()
                            .addToBackStack(null)
                            .replace(DEPOSIT_FRAGMENT_CONTAINER, confirmFragment, confirmFragment.TAG)
                            .commit();
                }
        }

    }


    public static String getName() {
        return TAG;
    }

    public  void doDepoInfoRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONArray data = resp.getJSONArray("data");
                                JSONObject dataObj;
                                for(int i = 0; i < data.length(); i++){
                                    dataObj = data.getJSONObject(i);
                                    DepositResponseVars drv = new DepositResponseVars(
                                            dataObj.getString("currency"),
                                            dataObj.getString("available"),
                                            dataObj.getString("busy"),
                                            dataObj.getString("expect")
                                    );
                                    itemList.add(new DepositComponentsList(i, drv.getCurrency(), drv.getAvaliable(), drv.getBusy(), drv.getExpect()));
                                    adapter1.notifyDataSetChanged();
                                }
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "available_funds");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
