package com.mobox.budabi.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.AllDepositAdapter;
import com.mobox.budabi.utils.AllDepositsComponents;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AllDepositsFragment extends Fragment {

    private ListView listView;
    private AllDepositAdapter adapter;
    private List<AllDepositsComponents> list;
    private User userInfo = UserLocalStore.getUserData();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_deposits, null);
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null){
            doAllDepositsRequest();
        }else {
            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
        }

        listView = (ListView) v.findViewById(R.id.allDepositsListView);
        list = new ArrayList<>();
        adapter = new AllDepositAdapter(list, getContext());
        listView.setAdapter(adapter);

        return v;
    }

    public  void doAllDepositsRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONArray data = resp.getJSONArray("data");
                                if(data.length() == 0){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    builder.setMessage("У вас нет активных депозитов.")
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    getActivity().onBackPressed();
                                                }
                                            });
                                    AlertDialog alertDialog = builder.create();
                                    alertDialog.show();
                                }else {
                                    for(int i=0; i < data.length(); i++){
                                        JSONObject dep = data.getJSONObject(i);
                                        int header = dep.getInt("dState");
                                        String startDate = dep.getString("dCTS");
                                        int currency = dep.getInt("dcID");
                                        String sum = dep.getString("dZD");
                                        String plan = dep.getString("pName");
                                        String lastCalculation = dep.getString("dLTS");
                                        String calculations = dep.getString("dN");
                                        String calculat = dep.getString("dZP");
                                        String kapnetZavtra = dep.getString("d");

                                        list.add(new AllDepositsComponents(
                                                header,
                                                startDate,
                                                currency,
                                                sum,
                                                plan,
                                                lastCalculation,
                                                calculations,
                                                calculat,
                                                kapnetZavtra
                                        ));
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "all_deposits");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
