package com.mobox.budabi.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.LayoutFragmentAdapter;
import com.mobox.budabi.utils.FragmentComponentsList;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VuvodConfirmFragment extends Fragment {

    public static final String TAG = "PayConfirmFragment";
    public static final int LIST_ITEM = R.layout.confirm_list_item;
    public static final int LIST_VIEW_ID = R.id.vuvod_confirm_list;
    private ListView listView;
    private Button submit;
    private Button cancel;
    private String currentTime = new SimpleDateFormat("HH:mm").format(new Date());
    private List<FragmentComponentsList> list;
    private LayoutFragmentAdapter mAdapter;
    private LinearLayout linearLayout;
    private String sum;
    private String paySystem;
    private String paySystemId;
    private User userInfo = UserLocalStore.getUserData();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_vuvod_confirm, null);
        Bundle bundle = getArguments();
        if(bundle != null){
            String[] s = bundle.getStringArray("vuvodData");
            sum = s[0];
            paySystem = s[1];
            paySystemId = getPaySystemId(paySystem);
        }

        listView = (ListView) v.findViewById(LIST_VIEW_ID);
        list = initItemList();
        mAdapter = new LayoutFragmentAdapter(getContext(), LIST_ITEM, list);
        listView.setAdapter(mAdapter);
        submit = (Button) v.findViewById(R.id.btn_vuvod_submit);
        linearLayout = (LinearLayout) v.findViewById(R.id.alertContainer);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        cancel = (Button) v.findViewById(R.id.btn_vuvod_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Выберите действие.")
                        .setMessage("Вы желаете отменить?")
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               getActivity().finish();
                            }
                        })
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        return v;
    }
    private void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Выберите действие.")
                .setMessage("Вы желаете подтвердить?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                        if (cm.getActiveNetworkInfo() != null) {
                            doVuvodRequest();
                        } else {
                            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void initDeleteBtn() {
        cancel.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
    }


    private List<FragmentComponentsList> initItemList() {
        List<FragmentComponentsList> list = new ArrayList<FragmentComponentsList>();
        list.add(new FragmentComponentsList(1, "Статус", "Ожидает подтверждения"));
        list.add(new FragmentComponentsList(2, "Дата", "сегодня," + currentTime));
        list.add(new FragmentComponentsList(3, "Платежная система", paySystem));
        list.add(new FragmentComponentsList(4, "Сумма", sum));
        return list;

    }

    private String getPaySystemId(String system){
        String res;
        switch (system){
            case "Perfect":
                res = "3";
                return res;
//            case "Гривна":
//                res = "6";
//                return res;
            case "Payeer":
                res = "4";
                return res;
//            case "Euro":
//                res = "2";
//                return res;
//            case "Злотых":
//                res = "8";
//                return res;
            default:
                res = "";
                return res;
        }
    }

    public static String getName() {
        return TAG;
    }

    public  void doVuvodRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);

                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");



                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                String currTime = new SimpleDateFormat("HH:mm").format(new Date());
                                list.add(new FragmentComponentsList(5, "Именено", "сегодня," + currTime));
                                mAdapter.notifyDataSetChanged();
                                initDeleteBtn();
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "operation_do");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                params.put("cid", paySystemId);
                params.put("sum", sum);
                params.put("oper", "CASHOUT");
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
