//package com.example.beelan.interInvest.fragments;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.net.ConnectivityManager;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.AlertDialog;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.example.beelan.interInvest.MyApplication;
//import com.example.beelan.interInvest.R;
//import com.example.beelan.interInvest.adapters.LayoutFragmentAdapter;
//import com.example.beelan.interInvest.utils.FragmentComponentsList;
//import com.example.beelan.interInvest.utils.ResponseErrorAnalytic;
//import com.example.beelan.interInvest.utils.User;
//import com.example.beelan.interInvest.utils.UserLocalStore;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class TransactionConfirmFragment extends Fragment {
//    public static final String TAG = "TransactionConfirmFragment";
//    public static final int LIST_ITEM = R.layout.confirm_list_item;
//    public static final int LIST_VIEW_ID = R.id.transaction_confirm_list;
//    public static final int BTN_TRANSACTION_CANCEL = R.id.btn_transaction_cancel;
//    public static final int BTN_TRANSACTION_SUBMIT = R.id.btn_transaction_submit;
//    private ListView listView;
//    private Button submit;
//    private Button cancel;
//    private String currentTime = new SimpleDateFormat("HH:mm").format(new Date());
//    private List<FragmentComponentsList> list;
//    private LayoutFragmentAdapter mAdapter;
//    private LinearLayout linearLayout;
//    private String sum;
//    private String paySystem;
//    private String reciever;
//    private String someInfo;
//    private String paySystemId;
//    private User userInfo = UserLocalStore.getUserData();
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_transaction_confirm, null);
//        Bundle bundle = getArguments();
//
//        if(bundle != null){
//            String[] s = bundle.getStringArray("transData");
//            sum = s[0];
//            reciever = s[1];
//            someInfo = s[2];
//            paySystem = s[3];
//        }
//        paySystemId = getPaySystemId(paySystem);
//
//        listView = (ListView) v.findViewById(LIST_VIEW_ID);
//        list = initItemList();
//        mAdapter = new LayoutFragmentAdapter(getContext(), LIST_ITEM, list);
//        listView.setAdapter(mAdapter);
//        submit = (Button) v.findViewById(BTN_TRANSACTION_SUBMIT);
//        linearLayout = (LinearLayout) v.findViewById(R.id.alertContainer);
//
//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDialog();
//            }
//        });
//
//        cancel = (Button) v.findViewById(BTN_TRANSACTION_CANCEL);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                builder.setTitle("Выбирите действие.")
//                        .setMessage("Вы желаете отменить?")
//                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                getActivity().finish();
//                            }
//                        })
//                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
//            }
//        });
//
//        return v;
//    }
//    private void showDialog() {
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle("Выбирите действие.")
//                .setMessage("Вы желаете подтвердить?")
//                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//                        if (cm.getActiveNetworkInfo() != null) {
//                            doTransactionRequest();
//                        } else {
//                            Toast.makeText(getContext(), "Подключнеие к интернету отсутствует.", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                })
//                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                });
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();
//    }
//
//    private void initDeleteBtn() {
//        cancel.setVisibility(View.GONE);
//        submit.setVisibility(View.GONE);
//        linearLayout.setVisibility(View.GONE);
//    }
//
//
//    private List<FragmentComponentsList> initItemList() {
//        List<FragmentComponentsList> list = new ArrayList<FragmentComponentsList>();
//        list.add(new FragmentComponentsList(1, "Статус", "Ожидает подтверждения"));
//        list.add(new FragmentComponentsList(2, "Дата", "сегодня, " + currentTime));
//        list.add(new FragmentComponentsList(3, "Получатель", reciever));
//        list.add(new FragmentComponentsList(4, "Платежная система", paySystem));
//        list.add(new FragmentComponentsList(5, "Сумма", sum));
//        list.add(new FragmentComponentsList(6, "Примечание", someInfo));
//        return list;
//
//    }
//
//    private String getPaySystemId(String system){
//        String res;
//        switch (system){
//            case "Dollar":
//                res = "3";
//                return res;
//            case "Гривна":
//                res = "6";
//                return res;
//            case "Рубли":
//                res = "4";
//                return res;
//            case "Euro":
//                res = "2";
//                return res;
//            case "Злотых":
//                res = "8";
//                return res;
//            default:
//                res = "";
//                return res;
//        }
//    }
//
//    public  void doTransactionRequest() {
//        String MASTER_URL = getString(R.string.master_url_for_server_request);
//        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.e("Response", response);
//                        try {
//                            JSONObject resp = new JSONObject(response);
//
//                            JSONObject status = resp.getJSONObject("status");
//                            int code = status.getInt("code");
//                            String massage = status.getString("message");
//
//                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
//                                JSONObject data = resp.getJSONObject("data");
//                                String batch = data.getString("oBatch");
//                                String currTime = new SimpleDateFormat("HH:mm").format(new Date());
//                                list.add(new FragmentComponentsList(7, "Именено", "сегодня, " + currTime));
//                                list.add(new FragmentComponentsList(8, "Batch-номер", batch));
//                                mAdapter.notifyDataSetChanged();
//                                initDeleteBtn();
//                            }else {
//                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.e("Error: " + error.toString());
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("q", "operation_do");
//                params.put("s1", userInfo.getBase64Token());
//                params.put("s2", userInfo.getTokenEncryptedLogin());
//                params.put("cid", paySystemId);
//                params.put("sum", sum);
//                params.put("login2", reciever);
//                params.put("oper", "TR");
//                params.put("memo", someInfo);
//                return params;
//            }
//
//        };
//
//        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
//
//    }
//}
