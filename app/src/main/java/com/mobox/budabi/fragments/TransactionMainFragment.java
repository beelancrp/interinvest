//package com.example.beelan.interInvest.fragments;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.example.beelan.interInvest.R;
//
//import fr.ganfra.materialspinner.MaterialSpinner;
//
//
//public class TransactionMainFragment extends Fragment {
//
//    private MaterialSpinner spinner;
//    private TransactionConfirmFragment confirmFragment = new TransactionConfirmFragment();
//    private FragmentManager mFragmentManager;
//    private Button submit;
//    private EditText sum;
//    private EditText reciever;
//    private EditText someInfo;
//    private String paySystem;
//    private String[] ITEMS = {"Dollar", "Гривна", "Рубли"};
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_trasaction_main, null);
//
//        mFragmentManager = getActivity().getSupportFragmentManager();
//
//        submit = (Button) v.findViewById(R.id.btnTransaction);
//        sum    = (EditText) v.findViewById(R.id.input_transaction_summ);
//        reciever = (EditText) v.findViewById(R.id.input_transaction_reciever);
//        someInfo = (EditText) v.findViewById(R.id.input_transaction_about);
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.drop_down_list, ITEMS);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.transaction_spinner);
//        spinner.setAdapter(adapter);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if(position != -1){
//                    paySystem = ITEMS[position];
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                paySystem = null;
//            }
//        });
//
//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onSubmit();
//            }
//        });
//
//        return v;
//    }
//
//    public void onSubmit() {
//        Bundle bundle = new Bundle();
//        String lSum = sum.getText().toString();
//        String lReciever = reciever.getText().toString();
//        String lSomeInfo = someInfo.getText().toString();
//
//        if (lSum != null && lReciever != null &&
//                lSomeInfo != null && paySystem != null){
//            bundle.putStringArray("transData", new String[]{lSum, lReciever, lSomeInfo, paySystem});
//            confirmFragment.setArguments(bundle);
//            mFragmentManager.beginTransaction()
//                    .addToBackStack(null)
//                    .replace(R.id.transaction_fragment_container, confirmFragment, confirmFragment.TAG)
//                    .commit();
//        }else {
//            Toast.makeText(getContext(), "Все поля должны быть заполнены.", Toast.LENGTH_LONG).show();
//        }
//
//    }
//
//}
