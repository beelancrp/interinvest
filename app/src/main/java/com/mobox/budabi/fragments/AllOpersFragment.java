package com.mobox.budabi.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.AllOpersAdapter;
import com.mobox.budabi.utils.AllOpersComponents;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AllOpersFragment extends Fragment{
    private AllOpersAdapter adapter;
    private List<AllOpersComponents> list;
    private User userInfo = UserLocalStore.getUserData();
    private ListView listView;
    private AllOpersMoreFragment mFragment = new AllOpersMoreFragment();
    private FragmentManager mFragmentManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_operations, null);
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        mFragmentManager = getActivity().getSupportFragmentManager();

        if(cm.getActiveNetworkInfo() != null){
            doAllOpersRequest();
        }else {
            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
        }

        list = new ArrayList<>();
        listView = (ListView) v.findViewById(R.id.allOperationsListView);
        adapter = new AllOpersAdapter(list, getContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String operId = list.get(position).getId();
                String defState = list.get(position).getState();
                Bundle bundle = new Bundle();
                bundle.putStringArray("operId", new String[]{operId, defState});
                mFragment.setArguments(bundle);
                mFragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.all_opers_fragment_container, mFragment)
                        .commit();
            }
        });

        return v;
    }

    public  void doAllOpersRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONArray data = resp.getJSONArray("data");
                                if(data.length() == 0){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    builder.setMessage("У вас нет активных операций.")
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    getActivity().finish();
                                                }
                                            });
                                }else {
                                    for(int i=0; i < data.length(); i++){
                                        JSONObject oper = data.getJSONObject(i);
                                        String header = oper.getString("oOper");
                                        String date = oper.getString("oTS");
                                        String currency = oper.getString("ocID");
                                        String sum = oper.getString("oSum");
                                        String state = oper.getString("oState");
                                        String id = oper.getString("oID");
                                        list.add(new AllOpersComponents(
                                                header,
                                                date,
                                                sum,
                                                currency,
                                                id,
                                                state
                                        ));
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "all_operations");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
