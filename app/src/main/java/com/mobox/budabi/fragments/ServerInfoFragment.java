package com.mobox.budabi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.mobox.budabi.R;

/**
 * Created by beeLAN on 09.12.2015.
 */
public class ServerInfoFragment extends Fragment {

    private Button ok;
    private WebView webView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_server_info, null);
        ok = (Button) v.findViewById(R.id.btn_return_to_main);
        webView = (WebView) v.findViewById(R.id.webView);
        webView.loadUrl("https://budabi.com/mob-text");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOk();
            }
        });
        return v;
    }

    public void onOk (){
        getActivity().finish();
    }
}
