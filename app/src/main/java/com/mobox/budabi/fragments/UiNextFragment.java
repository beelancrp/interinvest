package com.mobox.budabi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.utils.CallLoadnigView;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UiNextFragment extends Fragment {
    private RelativeLayout btnZlot;
    private RelativeLayout btnEuro;
    private RelativeLayout btnDoll;
    private RelativeLayout btnRubl;
    private RelativeLayout btnUah;
    private LinearLayout zlotContainer;
    private LinearLayout dollContainer;
    private LinearLayout euroContainer;
    private LinearLayout rublContainer;
    private LinearLayout uahContainer;
    private ImageView zlotArr;
    private ImageView dollArr;
    private ImageView euroArr;
    private ImageView uahArr;
    private ImageView rubArr;
    private User userInfo;
    private String perfectBanking;
    private String payeerBanking;
    private TextView perfectBankingTV;
    private TextView payerrBankingTV;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ui_next, null);
        initViews(v);
        userInfo = UserLocalStore.getUserData();
        doBillingInfoRequest();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Платежные реквизиты");
        perfectBankingTV.setText(perfectBanking);
        payerrBankingTV.setText(payeerBanking);

        btnZlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUIItemClick(v);
            }
        });
//        btnDoll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onUIItemClick(v);
//            }
//        });
        btnEuro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUIItemClick(v);
            }
        });
//        btnRubl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onUIItemClick(v);
//            }
//        });
//        btnUah.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onUIItemClick(v);
//            }
//        });

        return v;
    }

    private void initViews(View v) {
//        btnDoll = (RelativeLayout) v.findViewById(R.id.uiBtnDollar);
        btnZlot = (RelativeLayout) v.findViewById(R.id.uiBtnZlotuh);
        btnEuro = (RelativeLayout) v.findViewById(R.id.uiBtnEuro);
//        btnUah = (RelativeLayout) v.findViewById(R.id.uiBtnUah);
//        btnRubl = (RelativeLayout) v.findViewById(R.id.uiBtnRub);
        zlotContainer = (LinearLayout) v.findViewById(R.id.containerZlotuh);
        zlotContainer.setVisibility(View.GONE);
//        dollContainer = (LinearLayout) v.findViewById(R.id.containerDollar);
//        dollContainer.setVisibility(View.GONE);
        euroContainer = (LinearLayout) v.findViewById(R.id.containerEuro);
        euroContainer.setVisibility(View.GONE);
//        uahContainer = (LinearLayout) v.findViewById(R.id.containerUah);
//        uahContainer.setVisibility(View.GONE);
//        rublContainer = (LinearLayout) v.findViewById(R.id.containerRub);
//        rublContainer.setVisibility(View.GONE);
        zlotArr = (ImageView) v.findViewById(R.id.zlotArr);
//        dollArr = (ImageView) v.findViewById(R.id.dollArr);
        euroArr = (ImageView) v.findViewById(R.id.euroArr);
//        uahArr = (ImageView) v.findViewById(R.id.uahArr);
//        rubArr = (ImageView) v.findViewById(R.id.rubArr);
        perfectBankingTV = (TextView) v.findViewById(R.id.uibankANZ);
        payerrBankingTV = (TextView) v.findViewById(R.id.uiBankANE);
    }

    public void onUIItemClick(View v){
        switch (v.getId()){
            case R.id.uiBtnZlotuh:
                if (zlotContainer.getVisibility() != View.GONE){
                    zlotContainer.setVisibility(View.GONE);
                    zlotArr.setBackgroundResource(R.drawable.arrow_2);
                }else {
                    zlotContainer.setVisibility(View.VISIBLE);
                    zlotArr.setBackgroundResource(R.drawable.arrow_1);
                }
                break;
//            case R.id.uiBtnDollar:
//                if (dollContainer.getVisibility() != View.GONE){
//                    dollContainer.setVisibility(View.GONE);
//                    dollArr.setBackgroundResource(R.drawable.arrow_2);
//                }else {
//                    dollContainer.setVisibility(View.VISIBLE);
//                    dollArr.setBackgroundResource(R.drawable.arrow_1);
//                }
//                break;
            case R.id.uiBtnEuro:
                if (euroContainer.getVisibility() != View.GONE){
                    euroContainer.setVisibility(View.GONE);
                    euroArr.setBackgroundResource(R.drawable.arrow_2);
                }else {
                    euroContainer.setVisibility(View.VISIBLE);
                    euroArr.setBackgroundResource(R.drawable.arrow_1);
                }
                break;
//            case R.id.uiBtnUah:
//                if (uahContainer.getVisibility() != View.GONE){
//                    uahContainer.setVisibility(View.GONE);
//                    uahArr.setBackgroundResource(R.drawable.arrow_2);
//                }else {
//                    uahContainer.setVisibility(View.VISIBLE);
//                    uahArr.setBackgroundResource(R.drawable.arrow_1);
//                }
//                break;
//            case R.id.uiBtnRub:
//                if (rublContainer.getVisibility() != View.GONE){
//                    rublContainer.setVisibility(View.GONE);
//                    rubArr.setBackgroundResource(R.drawable.arrow_2);
//                }else {
//                    rublContainer.setVisibility(View.VISIBLE);
//                    rubArr.setBackgroundResource(R.drawable.arrow_1);
//                }
//                break;
        }
    }
    public  void doBillingInfoRequest() {
        final String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                JSONObject details = data.getJSONObject("details");
                                JSONObject perfect = details.getJSONObject("PERFECT");
                                if(perfect.getString("acc") == null){
                                    perfectBanking = "";
                                    perfectBankingTV.setText(perfectBanking);
                                }else {
                                    perfectBanking = perfect.getString("acc");
                                    perfectBankingTV.setText(perfectBanking);
                                }
                                JSONObject payeer = details.getJSONObject("PAYEER");
                                if (payeer.getString("acc") == null){
                                    payeerBanking = "";
                                    payerrBankingTV.setText(payeerBanking);
                                }else {
                                    payeerBanking = payeer.getString("acc");
                                    payerrBankingTV.setText(payeerBanking);
                                }
                                String exchange_rate = data.getString("exchange_rates");

                                CallLoadnigView.getInstance(getActivity(), R.id.cabinet_loginLoadingView).hideLoadingView();
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "billing_information");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());

                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
