package com.mobox.budabi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobox.budabi.R;

import fr.ganfra.materialspinner.MaterialSpinner;


public class PayMainFragment extends Fragment {


    public static final String TAG = "PayMainFragment";
    public static final int BTN_POPOLNENIE_SUBMIT = R.id.btn_popolnit;
    public static final int SPINNER = R.id.spinner;
    public static final int SIMPLE_SPINNER_DROPDOWN_ITEM = android.R.layout.simple_spinner_dropdown_item;
    public static final int DROP_DOWN_LIST = R.layout.drop_down_list;
    public static final int FRAGMENT_PAY_MAIN = R.layout.fragment_pay_main;
    public static final int INPUT_PAY_SYSTEM_NUMBER = R.id.input_pay_system_number;


    private PayConfirmFragment confirmFragment = new PayConfirmFragment();
    private FragmentManager mFragmentManager;
    private EditText paySumm;
    private Button submit;
    private String paySystem;
    private MaterialSpinner spinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(FRAGMENT_PAY_MAIN, null);

        final String[] ITEMS = {"Perfect", "Payeer"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), DROP_DOWN_LIST, ITEMS);
        adapter.setDropDownViewResource(SIMPLE_SPINNER_DROPDOWN_ITEM);
        spinner = (MaterialSpinner) view.findViewById(SPINNER);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1){
                    paySystem = ITEMS[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                    paySystem = null;
            }
        });
        paySumm = (EditText) view.findViewById(INPUT_PAY_SYSTEM_NUMBER);
        paySumm.clearFocus();
        mFragmentManager = getActivity().getSupportFragmentManager();
        submit = (Button) view.findViewById(BTN_POPOLNENIE_SUBMIT);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });

        return view;
    }

    public void onSubmit() {
        Bundle bundle = new Bundle();
        String summ = paySumm.getText().toString();
        if ((paySystem != null) && (!paySumm.getText().toString().isEmpty())){
            bundle.putStringArray("sysAndSum", new String[]{summ, paySystem});
            confirmFragment.setArguments(bundle);
            mFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.pay_system_fragment_container, confirmFragment, confirmFragment.getName())
                    .commit();
        }else {
            Toast.makeText(getContext(), "Все поля должны быть заполнены", Toast.LENGTH_LONG).show();
        }


    }

    public static String getName() {
        return TAG;
    }
}
