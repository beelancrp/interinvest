package com.mobox.budabi.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.LayoutFragmentAdapter;
import com.mobox.budabi.utils.AllOpersMoreComponents;
import com.mobox.budabi.utils.FragmentComponentsList;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AllOpersMoreFragment extends Fragment {
    private String operId;
    private String defState;
    private ListView listView;
    private LayoutFragmentAdapter adapter;
    private List<FragmentComponentsList>  list;
    private User userInfo = UserLocalStore.getUserData();
    private Button cencel;
    private Button delete;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_opers_more, null);
        Bundle bundle = getArguments();
        if(bundle != null){
            String[] s = bundle.getStringArray("operId");
            operId = s[0];
            defState = s[1];
        }
        cencel = (Button) v.findViewById(R.id.all_opers_more_cencel);
        cencel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Внимание")
                        .setMessage("Вы желаете отменить данную операцию?")
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                doOpersCencelDeleteRequest("cancel");
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                        })
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        delete = (Button) v.findViewById(R.id.all_opers_more_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Внимание")
                        .setMessage("Вы желаете удалить данную операцию?")
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                doOpersCencelDeleteRequest("del");
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                        })
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        switch (defState){
            case "0":
                cencel.setVisibility(View.VISIBLE);
                delete.setVisibility(View.GONE);
                break;
            case "1":
                cencel.setVisibility(View.VISIBLE);
                delete.setVisibility(View.GONE);
                break;
            case "2":
                cencel.setVisibility(View.VISIBLE);
                delete.setVisibility(View.GONE);
                break;
            case "3":
                cencel.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
                break;
            case "4":
                cencel.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
                break;
            case "5":
                cencel.setVisibility(View.GONE);
                delete.setVisibility(View.VISIBLE);
                break;
        }
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null){
            doAllOpersInfoRequest();
        }else {
            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();

        }

        listView = (ListView) v.findViewById(R.id.all_opers_confirm_list);
        list = new ArrayList<>();
        adapter = new LayoutFragmentAdapter(getContext(), R.layout.confirm_list_item, list);
        listView.setAdapter(adapter);
        return v;
    }
    public  void doAllOpersInfoRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                if (data.length() == 0){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                    builder.setMessage("Что то пошло не так")
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    getActivity().finish();
                                                }
                                            });
                                }else {
                                    if(data.has("oBatch") && data.has("oMemo")){
                                        AllOpersMoreComponents res = new AllOpersMoreComponents(
                                                data.getString("oOper"),
                                                data.getString("oState"),
                                                data.getString("oCTS"),
                                                data.getString("ocID"),
                                                data.getString("oSum"),
                                                data.getString("oBatch"),
                                                data.getString("oMemo"),
                                                data.getString("oTS"),
                                                defState);

                                        list.add(new FragmentComponentsList(1, "Тип операции", res.getOperation()));
                                        list.add(new FragmentComponentsList(2, "Статус", res.getState()));
                                        list.add(new FragmentComponentsList(3, "Дата", res.getDate()));
                                        list.add(new FragmentComponentsList(4, "Платежная система", res.getCurrency()));
                                        list.add(new FragmentComponentsList(5, "Сумма", res.getSun()));
                                        list.add(new FragmentComponentsList(6, "Batch-номер", res.getBatch()));
                                        list.add(new FragmentComponentsList(7, "Примечание", res.getDescr()));
                                        list.add(new FragmentComponentsList(8, "Изменено", res.getChanged()));
                                        adapter.notifyDataSetChanged();
                                    }else{
                                        AllOpersMoreComponents res = new AllOpersMoreComponents(
                                                data.getString("oOper"),
                                                data.getString("oState"),
                                                data.getString("oCTS"),
                                                data.getString("ocID"),
                                                data.getString("oSum"),
                                                data.getString("oTS"),
                                                defState);
                                        list.add(new FragmentComponentsList(1, "Тип операции", res.getOperation()));
                                        list.add(new FragmentComponentsList(2, "Статус", res.getState()));
                                        list.add(new FragmentComponentsList(3, "Дата", res.getDate()));
                                        list.add(new FragmentComponentsList(4, "Платежная система", res.getCurrency()));
                                        list.add(new FragmentComponentsList(5, "Сумма", res.getSun()));
                                        list.add(new FragmentComponentsList(6, "Примечание", res.getDescr()));
                                        list.add(new FragmentComponentsList(7, "Изменено", res.getChanged()));
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "operation_info");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                params.put("id", operId);
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    public  void doOpersCencelDeleteRequest(final String oper) {
        String MASTER_URL = "https://inter300.com/mob?";
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "operation_confirm");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                params.put("id", operId);
                params.put("do", oper);
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }
}
