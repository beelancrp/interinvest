package com.mobox.budabi.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class UserInfoFragment extends Fragment {

    private TextView email;
    private TextView login;
    private TextView lastname;
    private TextView country;
    private TextView name;
    private TextView city;
    private User userInfo = UserLocalStore.getUserData();
    private RelativeLayout relativeLayout;
    private FragmentManager mFragmentManager;
    private UiNextFragment fragment = new UiNextFragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_info, null);
        initLayoutViews(v);
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() != null){
            doUserInfoRequest();
        }else {
            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
        }
        mFragmentManager = getActivity().getSupportFragmentManager();
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.highlight_color));
                mFragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.user_info_fragment_container, fragment)
                        .commit();
            }
        });
        return v;
    }

    private void initLayoutViews(View view){
        email = (TextView) view.findViewById(R.id.uiuemail);
        login = (TextView) view.findViewById(R.id.uilogin);
        lastname = (TextView) view.findViewById(R.id.uilastname);
        country = (TextView) view.findViewById(R.id.uicountry);
        name = (TextView) view.findViewById(R.id.uiname);
        city = (TextView) view.findViewById(R.id.uicity);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.uipayrequisits);
    }

    public  void doUserInfoRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);

                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");


                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                email.setText(data.getString("email"));
                                login.setText(data.getString("login"));
                                lastname.setText(data.getString("last_name"));
                                country.setText(data.getString("country"));
                                name.setText(data.getString("name"));
                                city.setText(data.getString("city"));
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "profile");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Личные данные");
    }
}
