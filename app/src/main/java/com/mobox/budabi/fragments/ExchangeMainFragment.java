//package com.example.beelan.interInvest.fragments;
//
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.telecom.Call;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Request;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.example.beelan.interInvest.MyApplication;
//import com.example.beelan.interInvest.R;
//import com.example.beelan.interInvest.utils.CallLoadnigView;
//import com.example.beelan.interInvest.utils.ExchangeRates;
//import com.example.beelan.interInvest.utils.ResponseErrorAnalytic;
//import com.example.beelan.interInvest.utils.User;
//import com.example.beelan.interInvest.utils.UserLocalStore;
//
//import org.buraktamturk.loadingview.LoadingView;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.DecimalFormat;
//import java.util.HashMap;
//import java.util.Map;
//
//import fr.ganfra.materialspinner.MaterialSpinner;
//
//
//public class ExchangeMainFragment extends Fragment {
//    public static final String TAG = "ExchangeMainFragment";
//    public static final int FRAGMENT_EXCHANGE_MAIN = R.layout.fragment_exchange_main;
//    public static final int EXCHANGE_SPINNER_1 = R.id.exchange_spinner_1;
//    public static final int EXCHANGE_SPINNER_2 = R.id.exchange_spinner_2;
//    public static final int DROP_DOWN_LIST = R.layout.drop_down_list;
//    public static final int SIMPLE_SPINNER_DROPDOWN_ITEM = android.R.layout.simple_spinner_dropdown_item;
//    public static final int EXCHANGE_FRAGMENT_CONTAINER = R.id.exchange_fragment_container;
//
//    private ExchangeConfirmFragment confirmFragment = new ExchangeConfirmFragment();
//    private FragmentManager mFragmentManager;
//    private Button submit;
//    private TextView inputSum;
//    private TextView exchangedSum;
//    private String paySystemFrom;
//    private String paySystemTo;
//    private String[] ITEMS = {"Dollar"};
//    private String[] ITEMS1 = {"Гривна"};
//
//
//    private User userInfo = UserLocalStore.getUserData();
//    private ExchangeRates exchangeRates;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(FRAGMENT_EXCHANGE_MAIN, null);
//
//        submit = (Button) view.findViewById(R.id.btn_exchange);
//        inputSum = (TextView) view.findViewById(R.id.input_exchange_summ_1);
//        exchangedSum = (TextView) view.findViewById(R.id.exchangeSum);
//        mFragmentManager = getActivity().getSupportFragmentManager();
//        MaterialSpinner spinner1 = (MaterialSpinner) view.findViewById(EXCHANGE_SPINNER_1);
//        MaterialSpinner spinner2 = (MaterialSpinner) view.findViewById(EXCHANGE_SPINNER_2);
//
//        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        if (cm.getActiveNetworkInfo() != null){
//            CallLoadnigView.getInstance(view, R.id.exchange_loginLoadingView).showLoadingView();
//            doExchangeRatesInfoRequest();
//        }else {
//            Toast.makeText(getContext(),"Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
//            spinner1.setEnabled(false);
//            spinner2.setEnabled(false);
//            inputSum.setEnabled(false);
//        }
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), DROP_DOWN_LIST, ITEMS);
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), DROP_DOWN_LIST, ITEMS1);
//        adapter.setDropDownViewResource(SIMPLE_SPINNER_DROPDOWN_ITEM);
//        adapter1.setDropDownViewResource(SIMPLE_SPINNER_DROPDOWN_ITEM);
//        spinner2.setAdapter(adapter1);
//        spinner1.setAdapter(adapter);
//        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position != -1) {
//                    paySystemFrom = ITEMS[position];
//                    doExchangeOperation();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                paySystemFrom = null;
//            }
//        });
//        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position != -1){
//                    paySystemTo = "Гривна";
//                    doExchangeOperation();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                paySystemTo = null;
//            }
//        });
//
//        inputSum.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (inputSum.getText().toString().isEmpty()) {
//                    exchangedSum.setText("0");
//
//                } else {
//                    doExchangeOperation();
//
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                doExchangeOperation();
//            }
//        });
//
//        submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onSubmit();
//            }
//        });
//
//        return view;
//    }
//
//    public void onSubmit() {
//        Bundle bundle = new Bundle();
//        String summ = inputSum.getText().toString();
//        String exSumm = exchangedSum.getText().toString();
//        if (validate()){
//            bundle.putStringArray("data", new String[]{summ, paySystemFrom, paySystemTo, exSumm});
//            confirmFragment.setArguments(bundle);
//            mFragmentManager.beginTransaction()
//                    .addToBackStack(null)
//                    .replace(EXCHANGE_FRAGMENT_CONTAINER, confirmFragment, confirmFragment.TAG)
//                    .commit();
//        }else {
//            Toast.makeText(getContext(), "Все поля должны быть заполнены", Toast.LENGTH_LONG).show();
//        }
//
//
//    }
//
//    private boolean validate (){
//        if ((paySystemFrom != null) && (!inputSum.getText().toString().isEmpty()) && (paySystemTo != null)){
//            return true;
//        }else {return false;}
//    }
//
//    private void doExchangeOperation(){
//        double res = -1;
//        String in;
//        int summ;
//        String tmpExRate;
//        double exchangeRate = -1;
//        DecimalFormat df = new DecimalFormat("0.#");
//        if(validate()){
//            switch (paySystemFrom){
//                case "Dollar":
//                    in = inputSum.getText().toString();
//                    summ = Integer.parseInt(in);
//                    tmpExRate = exchangeRates.getUsd();
//                    exchangeRate = Double.parseDouble(tmpExRate);
//                    res = summ * exchangeRate;
//                    exchangedSum.setText(df.format(res));
//                    break;
//                case "Euro":
//                    in = inputSum.getText().toString();
//                    summ = Integer.parseInt(in);
//                    tmpExRate = exchangeRates.getEur();
//                    exchangeRate = Double.parseDouble(tmpExRate);
//                    res = summ * exchangeRate;
//                    exchangedSum.setText(df.format(res));
//                    break;
//            }
//
//        }
//
//    }
//
//
//    public static String getName() {
//        return TAG;
//    }
//
//    public  void doExchangeRatesInfoRequest() {
//        String MASTER_URL = getString(R.string.master_url_for_server_request);
//        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.e("Response", response);
//                        try {
//                            JSONObject resp = new JSONObject(response);
//
//                            JSONObject status = resp.getJSONObject("status");
//                            int code = status.getInt("code");
//                            String massage = status.getString("message");
//
//                            if (ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
//                                JSONObject data = resp.getJSONObject("data");
//                                exchangeRates = new ExchangeRates(
//                                        data.getString("USD"),
//                                        data.getString("EUR"),
//                                        data.getString("PLN"),
//                                        data.getString("RUB")
//                                );
//                                CallLoadnigView.getInstance(getView(), R.id.exchange_loginLoadingView).hideLoadingView();
//                            }else{
//                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext());
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.e("Error: " + error.toString());
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("q", "all_exchange_rates");
//                params.put("s1", userInfo.getBase64Token());
//                params.put("s2", userInfo.getTokenEncryptedLogin());
//                return params;
//            }
//
//        };
//
//        MyApplication.getInstance().addToRequestQueue(jsonObjReq);
//
//    }
//}
