package com.mobox.budabi.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.adapters.LayoutFragmentAdapter;
import com.mobox.budabi.utils.FragmentComponentsList;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PayConfirmFragment extends Fragment {

    public static final String TAG = "PayConfirmFragment";
    public static final int PAY_CONFIR_LIST_ITEM = R.layout.confirm_list_item;
    public static final int LIST_VIEW_ID = R.id.pay_system_confirm_list;
    private ListView listView;
    private Button submit;
    private Button cancel;
    private String currentTime = new SimpleDateFormat("HH:mm").format(new Date());
    private List<FragmentComponentsList> list;
    private LayoutFragmentAdapter mAdapter;
    private ImageView logo;
    private TextView alertMsg;
    private LinearLayout linearLayout;
    private String summ;
    private String paySystem;
    private String paySystemId;
    private User userInfo;
    Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pay_confirm, null);
        bundle = getArguments();

        if (bundle != null) {
            String[] s = bundle.getStringArray("sysAndSum");
            summ = s[0];
            paySystem = s[1];
        }
        switch (paySystem){
            case "Perfect":
                paySystemId = "3";
                break;
//            case "Гривна":
//                paySystemId = "6";
//                break;
            case "Payeer":
                paySystemId = "4";
                break;
//            case "Euro":
//                paySystemId = "2";
//                break;
//            case "Злотых":
//                paySystemId = "8";
//                break;
            default:
                paySystemId = "";
                break;
        }
        userInfo = UserLocalStore.getUserData();
        list = initItemList(summ, paySystem);
        listView = (ListView) v.findViewById(LIST_VIEW_ID);
        mAdapter = new LayoutFragmentAdapter(getContext(), PAY_CONFIR_LIST_ITEM, list);
        listView.setAdapter(mAdapter);

        submit = (Button) v.findViewById(R.id.btn_popolnenie_submit);
        logo = (ImageView) v.findViewById(R.id.pay_logo);
        alertMsg = (TextView) v.findViewById(R.id.pay_alert_msg);
        linearLayout = (LinearLayout) v.findViewById(R.id.alertContainer);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        cancel = (Button) v.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Выберите действие.")
                        .setMessage("Вы желаете отменить?")
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        return v;
    }

    private void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Выберите действие.")
                .setMessage("Вы желаете подтвердить?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                        if (cm.getActiveNetworkInfo() != null) {
                            doCabinetInfoRequest();
                        } else {
                            Toast.makeText(getContext(), "Подключение к интернету отсутствует.", Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void initDeleteBtn() {
        cancel.setVisibility(View.GONE);
        submit.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
    }


    private List<FragmentComponentsList> initItemList(String summ, String paySystem) {
        List<FragmentComponentsList> list = new ArrayList<FragmentComponentsList>();
            list.add(new FragmentComponentsList(1, "Статус", "Ожидает подтверждения"));
            list.add(new FragmentComponentsList(2, "Дата", "сегодня, " + currentTime));
            list.add(new FragmentComponentsList(3, "Платежная система", paySystem));
            list.add(new FragmentComponentsList(4, "Сумма", summ));
            return list;
    }

    public static String getName() {
        return TAG;
    }

    public  void doCabinetInfoRequest() {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);

                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if(ResponseErrorAnalytic.getInstance().responseIsPositive(code)){
                                JSONObject data = resp.getJSONObject("data");
                                String batch = data.getString("oBatch");
                                list.add(new FragmentComponentsList(5, "Batch-номер", batch));
                                mAdapter.notifyDataSetChanged();
                                initDeleteBtn();
                            }else {
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, getContext(), getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("q", "operation_do");
                params.put("s1", userInfo.getBase64Token());
                params.put("s2", userInfo.getTokenEncryptedLogin());
                params.put("cid", paySystemId);
                params.put("sum", summ);
                params.put("oper", "CASHIN");
                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

}
