package com.mobox.budabi.utils;

/**
 * Created by beeLAN on 19.11.2015.
 */
public class CabinetInfo {

//    private String usd;
//    private String uah;
    private String rus;
    private String eur;
//    private String pln;
//    private String exchange_rate;

    public CabinetInfo(String rus, String eur) {
//        this.usd = usd;
//        this.uah = uah;
        this.rus = rus;
        this.eur = eur;
//        this.pln = pln;
//        this.exchange_rate = exchange_rate;
    }

//    public String getUsd() {
//
//        return usd;
//    }
//
//    public String getUah() {
//        return uah;
//    }

    public String getRus() {
        return rus;
    }

    public String getEur() {
        return eur;
    }

//    public String getPln() {
//        return pln;
//    }
//
//    public String getExchange_rate() {
//        return exchange_rate;
//    }
}
