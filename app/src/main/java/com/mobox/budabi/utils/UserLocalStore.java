package com.mobox.budabi.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;

import com.mobox.budabi.MyApplication;


public class UserLocalStore {

    public static final String TOKEN = "token";
    public static final String LOGIN = "login";
    public static final String PASS = "password";
    public static final String LOGGED_IN = "loggedIn";


    private static SharedPreferences sharedPref;
    private static String token;
    private static String pushToken1;


    static{
        Context context = MyApplication.getInstance();
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static synchronized void setUserData(User data){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(LOGIN);
        editor.apply();
        editor.putString(TOKEN, data.getToken());
        editor.putString(LOGIN, data.getLogin());
        editor.putString(PASS, data.getPassword());
        editor.apply();
    }

    public static synchronized void setPushToken(String pushToken){
        pushToken1 = pushToken;
        SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("pushToken", Base64.encodeToString(pushToken.getBytes(), Base64.NO_WRAP))
                .apply();
    }

    public static String getPushToken(){
        return sharedPref.getString("pushToken", null);
    }

    public static User getUserData(){
        String token = sharedPref.getString(TOKEN, null);
        String login = sharedPref.getString(LOGIN, null);
        String pass  = sharedPref.getString(PASS, null);
        return new User(login, pass, token);
    }

    public static void setUserLoggedIn(boolean state){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(LOGGED_IN, state);
        editor.apply();
    }

    public static void cleanUserData(){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(TOKEN);
        editor.remove(PASS);
//        editor.commit();
    }

    public static boolean isUserLoggedIn(){
        if(sharedPref.getBoolean(LOGGED_IN, false) == true){
            return true;
        }else {
            return false;
        }

    }

}


