package com.mobox.budabi.utils;

import java.text.DecimalFormat;

/**
 * Created by beeLAN on 01.12.2015.
 */
public class AllOpersMoreComponents {
    private static final String BONUS = "BONUS";
    private static final String PENALTY = "PENALTY";
    private static final String CASHIN = "CASHIN";
    private static final String CASHOUT = "CASHOUT";
    private static final String EX = "EX";
    private static final String EXIN = "EXIN";
    private static final String TR = "TR";
    private static final String TRIN = "TRIN";
    private static final String BUY = "BUY";
    private static final String SELL = "SELL";
    private static final String BUY2 = "BUY2";
    private static final String SELL2 = "SELL2";
    private static final String REF = "REF";
    private static final String GIVE = "GIVE";
    private static final String TAKE = "TAKE";
    private static final String CALCIN = "CALCIN";
    private static final String CALCOUT = "CALCOUT";

    private String operation;
    private String state;
    private String date;
    private String currency;
    private String sun;
    private String batch;
    private String descr;
    private String changed;
    private String defaultState;

    public AllOpersMoreComponents(String operation, String state, String date, String currency, String sun, String batch, String descr, String changed, String defaultState) {
        this.operation = operation;
        this.state = state;
        this.date = date;
        this.currency = currency;
        this.sun = sun;
        this.batch = batch;
        this.descr = descr;
        this.changed = changed;
        this.defaultState = defaultState;
    }

    public AllOpersMoreComponents(String operation, String state, String date, String currency, String sun, String changed, String defaultState) {
        this.operation = operation;
        this.state = state;
        this.date = date;
        this.currency = currency;
        this.sun = sun;
        this.descr = descr;
        this.changed = changed;
        this.defaultState = defaultState;
    }

    public String getOperation() {
        String res = "";
        switch (operation){
            case BONUS:
                res = ("Бонус");
                break;
            case PENALTY:
                res = ("Штраф");
                break;
            case CASHIN:
                res = ("Пополнение");
                break;
            case CASHOUT:
                res = ("Вывод");
                break;
            case EX:
                res = ("Исходящий обмен");
                break;
            case EXIN:
                res = ("Входящий обмен");
                break;
            case TR:
                res = ("Перевод");
                break;
            case TRIN:
                res = ("Приход");
                break;
            case BUY:
                res = ("Продажа");
                break;
            case SELL:
                res = ("Продажа");
                break;
            case BUY2:
                res = ("Страховка");
                break;
            case SELL2:
                res = ("Благотворительность");
                break;
            case REF:
                res = ("Рефские");
                break;
            case GIVE:
                res = ("Вклад");
                break;
            case TAKE:
                res = ("Снятие");
                break;
            case CALCIN:
                res = ("Начисление");
                break;
            case CALCOUT:
                res = ("Отчисление");
                break;
        }
        return res;
    }

    public String getState() {
        String res = "";
        switch (operation){
            case "0":
                res = "Ожидает подтверждения";
                break;
            case "1":
                res = "Ожидает пополнения";
                break;
            case "2":
                res = "Ожидает выполнения";
                break;
            case "3":
                res = "Выполнена";
                break;
            case "4":
                res = "Отклонена";
                break;
            case "5":
                res = "Отменена";
                break;
            default:
                switch (defaultState) {
                    case "0":
                        res = "Ожидает подтверждения";
                        break;
                    case "1":
                        res = "Ожидает пополнения";
                        break;
                    case "2":
                        res = "Ожидает выполнения";
                        break;
                    case "3":
                        res = "Выполнена";
                        break;
                    case "4":
                        res = "Отклонена";
                        break;
                    case "5":
                        res = "Отменена";
                        break;
                }
        }
        return res;
    }

    public String getDate() {
        String year = date.substring(0, 4);
        String monthNumber = date.substring(4, 6);
        String month = "";
        String day = date.substring(6, 8);
        String timeH = date.substring(8, 10);
        String timeM = date.substring(9, 11);
        switch (monthNumber){
            case "01":
                month = "января";
                break;
            case "02":
                month = "февраля";
                break;
            case "03":
                month = "марта";
                break;
            case "04":
                month = "апреля";
                break;
            case "05":
                month = "мая";
                break;
            case "06":
                month = "июня";
                break;
            case "07":
                month = "июля";
                break;
            case "08":
                month = "августа";
                break;
            case "09":
                month = "сентября";
                break;
            case "10":
                month = "октября";
                break;
            case "11":
                month = "ноября";
                break;
            case "12":
                month = "декабря";
                break;
        }
        return day + " " + month + " " + year + "," + " " + timeH + ":" + timeM;
    }

    public String getCurrency() {
        String res ="";
        switch (currency){
            case "2":
                res = ("Евро");
                break;
            case "3":
                res = ("Доллары");
                break;
            case "4":
                res = ("Рубли");
                break;
            case "6":
                res = ("Гривны");
                break;
            case "8":
                res = ("Злотых");
                break;
        }
        return res;
    }

    public String getSun() {
        DecimalFormat df = new DecimalFormat("0.##");
        double sum = Double.parseDouble(sun);
        return df.format(sum);
    }

    public String getBatch() {
        return batch;
    }

    public String getDescr() {
        return descr;
    }

    public String getChanged() {
        String year = changed.substring(0, 4);
        String monthNumber = changed.substring(4, 6);
        String month = "";
        String day = changed.substring(6, 8);
        String timeH = changed.substring(8, 10);
        String timeM = changed.substring(9, 11);
        switch (monthNumber){
            case "01":
                month = "января";
                break;
            case "02":
                month = "февраля";
                break;
            case "03":
                month = "марта";
                break;
            case "04":
                month = "апреля";
                break;
            case "05":
                month = "мая";
                break;
            case "06":
                month = "июня";
                break;
            case "07":
                month = "июля";
                break;
            case "08":
                month = "августа";
                break;
            case "09":
                month = "сентября";
                break;
            case "10":
                month = "октября";
                break;
            case "11":
                month = "ноября";
                break;
            case "12":
                month = "декабря";
                break;
        }
        return day + " " + month + " " + year + "," + " " + timeH + ":" + timeM;
    }
}
