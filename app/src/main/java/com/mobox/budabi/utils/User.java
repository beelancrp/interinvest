package com.mobox.budabi.utils;


import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Base64;

import com.mobox.budabi.MyApplication;

import java.util.HashMap;
import java.util.Map;

public class User {
    private static final int REQUEST_READ_PHONE_STATE_PERMISSION = 225;
    TelephonyManager tManager;
    private String  uuid = "";
    private String login = "";
    private String password = "";
    private String token = "";
    private String key = "DFSNJEWFGHDFHG564356347E23D2E8";
    private String compliteKey = "";
    private String device = "2";

    public User (String login, String password, Context context, Activity activity){
        this.login = login;
        this.password = password;
//        if(Build.VERSION.SDK_INT == Build.VERSION_CODES.M){
//            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                tManager = (TelephonyManager) MyApplication.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
//                uuid = tManager.getDeviceId();
//            } else {
//                activity.requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
//                        REQUEST_READ_PHONE_STATE_PERMISSION);
//            }
//        }else {
//        }
        tManager = (TelephonyManager) MyApplication.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
        uuid = tManager.getDeviceId();

        compliteKey = uuid + key;
    }
    public User (String login, String password, String token){
        this.login = login;
        this.password = password;
        this.token = token;
        compliteKey = uuid + key;
    }

    public void setToken (String token){
        this.token = token;
    }

    public String getToken(){
        return token;
    }

    public String getBase64Token(){
        return Base64.encodeToString(token.getBytes(), Base64.NO_WRAP);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getKey() {
        return key;
    }

    public String getCompliteKey() {
        return compliteKey;
    }

    private String getEncryptUUID(){
        String res = XOR.getEncryptString(uuid, key);
        return res;
    }

    private String getEncryptLogin(){
        String res = XOR.getEncryptString(login, compliteKey);
        return res;
    }

    private String getEncryptPass(){
        String res = XOR.getEncryptString(password, compliteKey);
        return res;
    }

    public String getTokenEncryptedLogin(){
        String res = XOR.getEncryptString(login, token);
        return res;
    }


    public Map<String, String> getParamsForLoginRequest(){
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", "login");
        params.put("u0", getEncryptUUID());
        params.put("u1", getEncryptLogin());
        params.put("u2", getEncryptPass());
        params.put("u3", UserLocalStore.getPushToken());
        params.put("u4", Base64.encodeToString(device.getBytes(), Base64.NO_WRAP));
        return params;
    }

    public Map<String, String> getParamsForCabinetRequest(){
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", "cabinet");
        params.put("s1", Base64.encodeToString(token.getBytes(), Base64.NO_WRAP));
        params.put("s2", getTokenEncryptedLogin());
        return params;
    }
}
