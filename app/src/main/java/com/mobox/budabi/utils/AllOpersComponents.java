package com.mobox.budabi.utils;


import java.text.DecimalFormat;

public class AllOpersComponents {
    private String header;
    private String date;
    private String sum;
    private String currency;
    private String id;
    private String state;

    public AllOpersComponents(String header, String date, String sum, String currency, String id, String state) {
        this.header = header;
        this.date = date;
        this.sum = sum;
        this.currency = currency;
        this.id = id;
        this.state = state;
    }

    public String getHeader() {
        return header;
    }

    public String getDate() {
        String year = date.substring(0, 4);
        String monthNumber = date.substring(4, 6);
        String month = "";
        String day = date.substring(6, 8);
        String time = date.substring(8);
        switch (monthNumber){
            case "01":
                month = "января";
                break;
            case "02":
                month = "февраля";
                break;
            case "03":
                month = "марта";
                break;
            case "04":
                month = "апреля";
                break;
            case "05":
                month = "мая";
                break;
            case "06":
                month = "июня";
                break;
            case "07":
                month = "июля";
                break;
            case "08":
                month = "августа";
                break;
            case "09":
                month = "сентября";
                break;
            case "10":
                month = "октября";
                break;
            case "11":
                month = "ноября";
                break;
            case "12":
                month = "декабря";
                break;
        }
        return day + " " + month + " " + year;
    }

    public String getSum() {
        double summ = Double.parseDouble(sum);
        DecimalFormat df = new DecimalFormat("0.##");
        return df.format(summ);
    }

    public String getCurrency() {
        return currency;
    }

    public String getId() {
        return id;
    }

    public String getState() {
        return state;
    }

}
