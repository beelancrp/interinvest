package com.mobox.budabi.utils;

import android.app.Activity;
import android.view.View;

import org.buraktamturk.loadingview.LoadingView;

public class CallLoadnigView {

    private static LoadingView mLoadingView;

    private static CallLoadnigView ourInstance = new CallLoadnigView();

    public static CallLoadnigView getInstance(Activity context, int rId) {
        mLoadingView = (LoadingView) context.findViewById(rId);
        return ourInstance;
    }
    public static CallLoadnigView getInstance(View context, int rId) {
        mLoadingView = (LoadingView) context.findViewById(rId);
        return ourInstance;
    }

    private CallLoadnigView() {
    }

    public void showLoadingView(){
        if(!mLoadingView.isShown()){
            mLoadingView.setLoading(true);
        }
    }

    public void hideLoadingView(){
        if(mLoadingView.isShown()){
            mLoadingView.setLoading(false);
        }
    }
}
