package com.mobox.budabi.utils;

/**
 * Created by beeLAN on 23.11.2015.
 */
public class ExchangeRates {
    private String usd, eur, pln, rub;

    public ExchangeRates(String usd, String eur, String pln, String rub) {
        this.usd = usd;
        this.eur = eur;
        this.pln = pln;
        this.rub = rub;
    }

    public String getUsd() {
        return usd;
    }

    public String getEur() {
        return eur;
    }

    public String getPln() {
        return pln;
    }

    public String getRub() {
        return rub;
    }
}
