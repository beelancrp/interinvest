package com.mobox.budabi.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.mobox.budabi.MainActivity;

public class ResponseErrorAnalytic {
    private static ResponseErrorAnalytic ourInstance = new ResponseErrorAnalytic();

    public static ResponseErrorAnalytic getInstance() {
        return ourInstance;
    }

    private ResponseErrorAnalytic() {
    }

     final String PAY_SYSTEM_ERROR = "pays_empty";
     final String WRONG_SUM = "sum_wrong";
     final String LIMIT_EXCEEDED = "limit_exceeded";
     final String WRONG_PIN = "pin_wrong";
     final String LOW_BALANCE = "low_bal1";
     final String OPERATION_DISABLE = "oper_disabled";
     final String MORE_THEN_MAX_SUM = "sum_max";
     final String LESS_THEN_MIN_SUM = "sum_min";
     final String WRONG_PLAN = "plan_wrong";
     final String RECIEVER_NOT_FOUND = "user2_empty";
     final String LOGIN_EMPTY = "login_empty";
     final String LOGIN_NOT_FOUND = "login_not_found";
     final String LOGIN_NOT_ACTIVE = "not_active";
     final String ADMIN_USER_BAN = "banned";
     final String USER_BLOCKED = "blocked";
     final String OPER_NOT_FOUND = "operation_not_found";
     final String EXCHANGE_DISABLE = "this_exchange_disabled";
     final String DEPO_NOT_FOUND = "depo_not_found";
     final String CASHOUT_ONLY_ON_MONDAY = "cashout_only_on_monday";
     boolean triger = false;

    public boolean responseIsPositive(int code){
        if (code == 30){
            return false;
        }else {
            return true;
        }
    }

    public void errorMsgsHandler(String message, final Context context, final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog alertDialog;
        switch (message){
            case PAY_SYSTEM_ERROR:
                final boolean res = false;
                builder.setTitle("Предупреждение")
                        .setMessage("Ошибка платежной системы. Убедитесь что выбрана правильная платежная система")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case WRONG_SUM:
                builder.setTitle("Предупреждение")
                        .setMessage("Убедитесь что сумма введена верно.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LIMIT_EXCEEDED:
                builder.setTitle("Предупреждение")
                        .setMessage("Вы привысили лимит.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case OPERATION_DISABLE:
                builder.setTitle("Предупреждение")
                        .setMessage("Операция запрещена.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case WRONG_PIN:
                builder.setTitle("Предупреждение")
                        .setMessage("Не верный пин.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOW_BALANCE:
                builder.setTitle("Предупреждение")
                        .setMessage("У вас недостаточно средств.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case MORE_THEN_MAX_SUM:
                builder.setTitle("Предупреждение")
                        .setMessage("Введенная сумма привышает максимальную.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LESS_THEN_MIN_SUM:
                builder.setTitle("Предупреждение")
                        .setMessage("Введенная сумма ниже минимальной.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case RECIEVER_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Пользователь для перевода не указан или указан не верно.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOGIN_EMPTY:
                builder.setTitle("Предупреждение")
                        .setMessage("Логин не указан.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOGIN_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Вы ввели неверный логин или пароль.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                                Intent intent = new Intent(context, MainActivity.class);
                                context.startActivity(intent);
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOGIN_NOT_ACTIVE:
                builder.setTitle("Предупреждение")
                        .setMessage("Учетная запись не активирована.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case ADMIN_USER_BAN:
                builder.setTitle("Предупреждение")
                        .setMessage("Учетная запись заблокирована администратором.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case USER_BLOCKED:
                builder.setTitle("Предупреждение")
                        .setMessage("Учетная запись заблокирована.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case OPER_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Операция не найдена.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case DEPO_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Депозит не найден.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case EXCHANGE_DISABLE:
                builder.setTitle("Предупреждение")
                        .setMessage("Этот обмен запрещен.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case CASHOUT_ONLY_ON_MONDAY:
                builder.setTitle("Предупреждение")
                        .setMessage("Вывод только в воскресенье, с 9 до 18.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case WRONG_PLAN:
                builder.setTitle("Предупреждение")
                        .setMessage("Не верный план.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                activity.finish();
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            default:

        }
    }

    public void errorMsgsHandler(String message, final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog alertDialog;
        switch (message){
            case PAY_SYSTEM_ERROR:
                final boolean res = false;
                builder.setTitle("Предупреждение")
                        .setMessage("Ошибка платежной системы. Убедитесь что выбрана правильная платежная система")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case WRONG_SUM:
                builder.setTitle("Предупреждение")
                        .setMessage("Убедитесь что сумма введена верно.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LIMIT_EXCEEDED:
                builder.setTitle("Предупреждение")
                        .setMessage("Вы привысили лимит.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case OPERATION_DISABLE:
                builder.setTitle("Предупреждение")
                        .setMessage("Операция запрещена.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case WRONG_PIN:
                builder.setTitle("Предупреждение")
                        .setMessage("Не верный пин.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOW_BALANCE:
                builder.setTitle("Предупреждение")
                        .setMessage("У вас недостаточно средств.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case MORE_THEN_MAX_SUM:
                builder.setTitle("Предупреждение")
                        .setMessage("Введенная сумма привышает максимальную.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LESS_THEN_MIN_SUM:
                builder.setTitle("Предупреждение")
                        .setMessage("Введенная сумма ниже минимальной.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case RECIEVER_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Пользователь для перевода не указан или указан не верно.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOGIN_EMPTY:
                builder.setTitle("Предупреждение")
                        .setMessage("Логин не указан.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOGIN_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Вы ввели неверный логин или пароль.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Intent intent = new Intent(context, MainActivity.class);
//                                context.startActivity(intent);
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case LOGIN_NOT_ACTIVE:
                builder.setTitle("Предупреждение")
                        .setMessage("Учетная запись не активирована.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case ADMIN_USER_BAN:
                builder.setTitle("Предупреждение")
                        .setMessage("Учетная запись заблокирована администратором.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case USER_BLOCKED:
                builder.setTitle("Предупреждение")
                        .setMessage("Учетная запись заблокирована.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case OPER_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Операция не найдена.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case DEPO_NOT_FOUND:
                builder.setTitle("Предупреждение")
                        .setMessage("Депозит не найден.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case EXCHANGE_DISABLE:
                builder.setTitle("Предупреждение")
                        .setMessage("Этот обмен запрещен.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case CASHOUT_ONLY_ON_MONDAY:
                builder.setTitle("Предупреждение")
                        .setMessage("Вывод только в воскресенье, с 9 до 18.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case WRONG_PLAN:
                builder.setTitle("Предупреждение")
                        .setMessage("Не верный план.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            default:

        }
    }
}
