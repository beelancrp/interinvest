package com.mobox.budabi.utils;

import java.text.DecimalFormat;

/**
 * Created by beeLAN on 07.12.2015.
 */
public class DepositResponseVars {
    String currency;
    String avaliable;
    String busy;
    String expect;

    public DepositResponseVars(String currency, String avaliable, String busy, String expect) {
        this.currency = currency;
        this.avaliable = avaliable;
        this.busy = busy;
        this.expect = expect;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAvaliable() {
        double res = Double.parseDouble(avaliable);
        DecimalFormat df = new DecimalFormat("0.##");
        return df.format(res);
    }

    public String getBusy() {
        double res = Double.parseDouble(busy);
        DecimalFormat df = new DecimalFormat("0.##");
        return df.format(res);
    }

    public String getExpect() {
        double res = Double.parseDouble(expect);
        DecimalFormat df = new DecimalFormat("0.##");
        return df.format(res);
    }
}
