package com.mobox.budabi.utils;

import java.text.DecimalFormat;

/**
 * Created by beeLAN on 09.12.2015.
 */
public class NewDepoResponse {
    String sum;
    String paySystem;
    String calculate;

    public NewDepoResponse(String sum, String paySystem, String calculate) {
        this.sum = sum;
        this.paySystem = paySystem;
        this.calculate = calculate;
    }

    public String getSum() {
        double res = Double.parseDouble(sum);
        DecimalFormat df = new DecimalFormat("0.##");
        return df.format(res);
    }

    public String getPaySystem() {
        return paySystem;
    }

    public String getCalculate() {
        return calculate;
    }
}
