package com.mobox.budabi.utils;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.EditText;

import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;


public class CustomEditText extends EditText {
    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (getText().length() > 0 && focused) {
            Drawable dw = MyApplication.getInstance().getResources().getDrawable(R.drawable.cancel);
            setCompoundDrawablesWithIntrinsicBounds(null, null, dw, null);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int DRAWABLE_RIGHT = 2;
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (getCompoundDrawables()[DRAWABLE_RIGHT] != null) {
                if (event.getRawX() >= (getRight() - getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()-40)) {
                    setText("");
                    setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }
        }
        return super.onTouchEvent(event);
    }


    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void addTextChangedListener(TextWatcher watcher) {

        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (getText().length() != 0 && isFocused()) {
                    Drawable dw = MyApplication.getInstance().getResources().getDrawable(R.drawable.cancel);
                    setCompoundDrawablesWithIntrinsicBounds(null, null, dw, null);
                } else {
                    setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }

            }
        };

        super.addTextChangedListener(tw);
    }

}
