package com.mobox.budabi.utils;


import android.util.Base64;

public class XOR {
    private static XOR ourInstance = new XOR();

    public static XOR getInstance() {
        return ourInstance;
    }

    private XOR() {
    }

    public static String getEncryptString(String text, String keyWord){
        byte[] arr = encrypt(text, keyWord);
        return Base64.encodeToString(arr, Base64.NO_WRAP);
    }

    public static String getDecryptString(String text, String keyWord){
        byte[] txt = Base64.decode(text, Base64.NO_WRAP);
        return decrypt(txt, keyWord);
    }

    private static byte[] encrypt(String text, String keyWord){
        byte[] arr = text.getBytes();
        byte[] keyarr = keyWord.getBytes();
        byte[] result = new byte[arr.length];
        for(int i = 0; i< arr.length; i++) {
            result[i] = (byte) (arr[i] ^ keyarr[i % keyarr.length]);
        }
        return result;
    }

    private static String decrypt(byte[] text, String keyWord){
        byte[] result  = new byte[text.length];
        byte[] keyarr = keyWord.getBytes();
        for(int i = 0; i < text.length;i++)
        {
            result[i] = (byte) (text[i] ^ keyarr[i% keyarr.length]);
        }
        return new String(result);
    }
}
