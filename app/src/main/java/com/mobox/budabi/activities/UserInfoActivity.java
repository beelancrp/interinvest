package com.mobox.budabi.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mobox.budabi.R;
import com.mobox.budabi.fragments.UserInfoFragment;

public class UserInfoActivity extends AppCompatActivity {
    public static final int IC_KEYBOARD_BACKSPACE         = R.drawable.ic_arrow_left;

    private Toolbar toolBar;
    private FragmentManager mFragmentManager;
    private UserInfoFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        initToolBar();
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }

        });

        mFragmentManager = getSupportFragmentManager();
        mFragment = new UserInfoFragment();
        mFragmentManager.beginTransaction().addToBackStack(null).replace(R.id.user_info_fragment_container, mFragment)
                .commit();

    }

    private void initToolBar() {
        toolBar = (Toolbar) findViewById(R.id.user_info_toolbar);
        toolBar.setNavigationIcon(IC_KEYBOARD_BACKSPACE);
        toolBar.setTitle("Личные данные");
        setSupportActionBar(toolBar);
    }

    @Override
    public void onBackPressed() {
        int getCountFragm = mFragmentManager.getBackStackEntryCount();
        if (getCountFragm > 1) {
                mFragmentManager.popBackStack();
        } else{
            finish();
        }
    }
}
