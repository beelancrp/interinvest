package com.mobox.budabi.activities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.mobox.budabi.Manifest;
import com.mobox.budabi.R;
import com.mobox.budabi.MyApplication;
import com.mobox.budabi.fragments.ServerInfoFragment;
import com.mobox.budabi.utils.RegistrationIntentService;
import com.mobox.budabi.utils.ResponseErrorAnalytic;
import com.mobox.budabi.utils.User;
import com.mobox.budabi.utils.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static android.Manifest.permission;
import static android.widget.Toast.LENGTH_LONG;


public class LoginActivity extends AppCompatActivity {

    //--Const. with view id.
    private static final String TAG = "LoginActivity";
    private static final int INPUT_LOGIN = R.id.input_email;
    private static final int INPUT_PASSWORD = R.id.input_password;
    private static final int BTN_LOGIN = R.id.btn_login;
    private static final int TI_LOGIN = R.id.TI_email;
    private static final int TI_PASS = R.id.TI_pass;
    private static final int ACTIVITY_LOGIN = R.layout.activity_login;
    private static final int REQUEST_READ_PHONE_STATE_PERMISSION = 225;


    //--Views instance declaration.
    private EditText loginText;
    private EditText passwordText;
    private Button loginButton;
    private TextInputLayout loginTextInput;
    private TextInputLayout passwordTextInput;
    private RelativeLayout mainWindow;
    private TextView regitrationText;
    private ProgressDialog progressDialog;
    private User userInfo;
    private FragmentManager fragmentManager;
    private ServerInfoFragment infoFragment = new ServerInfoFragment();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    private GoogleApiClient client;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(ACTIVITY_LOGIN);

        fragmentManager = getSupportFragmentManager();

//------Init views----------------------------------------------------------//
        loginText = (EditText) findViewById(INPUT_LOGIN);
        passwordText = (EditText) findViewById(INPUT_PASSWORD);
        loginButton = (Button) findViewById(BTN_LOGIN);
        loginTextInput = (TextInputLayout) findViewById(TI_LOGIN);
        passwordTextInput = (TextInputLayout) findViewById(TI_PASS);
        mainWindow = (RelativeLayout) findViewById(R.id.login_fragment_container);
        regitrationText = (TextView) findViewById(R.id.registrationText);
//--------------------------------------------------------------------------//

        regitrationText.setText(Html.fromHtml("<a href=\"https://budabi.com/registration\">Зарегистрироваться через сайт</a>"));
        regitrationText.setMovementMethod(LinkMovementMethod.getInstance());

        if (UserLocalStore.getUserData() != null) {
            User tmp = UserLocalStore.getUserData();
            loginText.setText(tmp.getLogin());
        }

        if(Build.VERSION.SDK_INT == Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.C2D_MESSAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.C2D_MESSAGE, permission.READ_PHONE_STATE},
                        123);
            }
        }else {
            Intent intent = new Intent(LoginActivity.this, RegistrationIntentService.class);
            startService(intent);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        mainWindow.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(LoginActivity.this);
                return false;
            }
        });

    }

    public void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

        loginButton.setEnabled(false);

        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog_M);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Идет авторизация...");
        progressDialog.setCancelable(false);
        showpDialog();

        final String email = loginText.getText().toString();
        final String password = passwordText.getText().toString();
        userInfo = new User(email, password, this, LoginActivity.this);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null) {
            doLoginRequest(userInfo.getParamsForLoginRequest());
            return;
        } else {
            Toast.makeText(this, "Подключение к интернету отсутствует.", LENGTH_LONG).show();
            hidepDialog();
            loginButton.setEnabled(true);
        }

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess(String token) {
        userInfo.setToken(token);
        loginButton.setEnabled(true);
        UserLocalStore.setUserData(userInfo);
        UserLocalStore.setUserLoggedIn(true);
        setResult(RESULT_OK);
        fragmentManager.beginTransaction()
                .add(R.id.login_fragment_container, infoFragment)
                .commit();
        hidepDialog();
    }

    public void onLoginFailed() {
//        Toast.makeText(getBaseContext(), "Login failed", LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = loginText.getText().toString();
        String password = passwordText.getText().toString();
        if (email.isEmpty()) {
            loginTextInput.setError("введите правильный логин или e-mail");
            loginTextInput.setErrorEnabled(true);
            valid = false;
        } else {
            loginText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 20) {
            passwordTextInput.setError("пароль должен содержать минимум 4 символа");
            passwordTextInput.setErrorEnabled(true);
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Proceed to next steps

                } else {
                }
                return;
            case 123:
                Intent intent = new Intent(LoginActivity.this, RegistrationIntentService.class);
                startService(intent);
                return;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void doLoginRequest(final Map<String, String> params) {
        String MASTER_URL = getString(R.string.master_url_for_server_request);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST, MASTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject resp = new JSONObject(response);
                            JSONObject status = resp.getJSONObject("status");
                            int code = status.getInt("code");
                            String massage = status.getString("message");

                            if (ResponseErrorAnalytic.getInstance().responseIsPositive(code)) {
                                JSONObject data = resp.getJSONObject("data");
                                String token = data.getString("token");
                                onLoginSuccess(token);
                            } else {
                                hidepDialog();
                                ResponseErrorAnalytic.getInstance().errorMsgsHandler(massage, LoginActivity.this);
                                onLoginFailed();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: " + error.toString());
                Toast toast = Toast.makeText(LoginActivity.this, "Извините, в данный момент сервер недоступен.", LENGTH_LONG);
                toast.show();
                hidepDialog();
                loginButton.setEnabled(true);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

        };

        MyApplication.getInstance().addToRequestQueue(jsonObjReq);

    }

    private void showpDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    private void hidepDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
